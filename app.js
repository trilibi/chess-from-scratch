let board = [
  [[0,4], [0,5], [0,3], [0,1], [0,2], [0,3], [0,5], [0,4]],
  [[0,0], [0,6], [0,6], [0,6], [0,6], [0,6], [0,6], [0,6]],
  [[0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0]],
  [[0,6], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0]],
  [[0,0], [1,6], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0]],
  [[0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0]],
  [[1,6], [0,0], [1,6], [1,6], [1,6], [1,6], [1,6], [1,6]],
  [[1,4], [1,5], [1,3], [1,2], [1,1], [1,3], [1,5], [1,4]]
];

const zero = () => {
  return [0, 0]
};

const pieces = {
  0 : '•',
  1 : 'K',
  2 : 'Q',
  3 : 'B',
  4 : 'R',
  5 : 'N',
  6 : 'P',
  7 : 'X'
};
const piecesW = {
  0 : '•',
  1 : '♔',
  2 : '♕',
  3 : '♗',
  4 : '♖',
  5 : '♘',
  6 : '♙',
  7 : 'X'
};
const piecesB = {
  0 : '•',
  1 : '♚',
  2 : '♛',
  3 : '♝',
  4 : '♜',
  5 : '♞',
  6 : '♟',
  7 : 'X'
};

const piecesT = {
  0 : '•',
  1 : 'King',
  2 : 'Queen',
  3 : 'Bishop',
  4 : 'Rook',
  5 : 'Knight',
  6 : 'Pawn',
  7 : 'X'
};

const rowN = {
  0 : 'H',
  1 : 'G',
  2 : 'F',
  3 : 'E',
  4 : 'D',
  5 : 'C',
  6 : 'B',
  7 : 'A'
};
const AlphaToNumber = {
  'h' : 0,
  'g' : 1,
  'f' : 2,
  'e' : 3,
  'd' : 4,
  'c' : 5,
  'b' : 6,
  'a' : 7
};

const spacer = '         ';
const spacerr = '        ';

function printRow(row) {
  console.log(spacer + row.join(""));
}

function printHeader(isFooter = false) {
  isFooter || console.log('');
  !isFooter || console.log('');
  console.log(spacerr + [' ', '', 1, 2, 3, 4, 5, 6, 7, 8, '', ' '].join('  '));
  isFooter || console.log('');
  !isFooter || console.log('');
}

const reset = "\x1b[0m";
const normalColor = "\x1b[0m";
const bgWhite = "\x1b[47m";
const bgBlack = "\x1b[40m";

function meep(row, col, i, highlight) {
  let name = pieces[i[1]];
  let bg = ((row%2 === 0 && col%2 === 0) || (row%2 === 1 && col%2 === 1)) ? bgWhite : bgBlack;
  let owner = i[0] ? "\x1b[97m" : "\x1b[97m";

//  console.log('XXX', highlight, !highlight || highlight[0] == row)
  if (highlight && highlight[0] == row && highlight[1] == col) {
    owner = "\x1b[41m";
  }

  if (i[1]===0) { return bg + '   ';}
  return bg + owner + " " + (i[0] ? piecesB[i[1]] : piecesW[i[1]]) + " " + reset;
}

function display(highlight = false) {

  printHeader();

  for ( let rowId in board) {
    let row = board[rowId];
    let rowV = [];
    rowV.push(reset + rowN[rowId] + "  ");
    rowV.push(reset + '');
    row.map((p, colId) => {
      rowV.push(meep(rowId, colId, p, highlight));
    });
    rowV.push(reset + '');
    rowV.push(reset + "  " +  rowN[rowId]);
    printRow(rowV);
  }
  ;

  printHeader(true);
}


function getPoint(row, col) {
  return (board[row][col][0] ? 'white' : 'black') + ' ' + piecesT[board[row][col][1]];
}

console.clear();

console.log("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
console.log('init');
display();


const OriginalAllTheMoves = {
  P: [
    [1, 0],
    [2, 0],
    [1,-1],
    [1, 1]
  ],
  Q: [
    ['X','X'],
    ['X', 0],
    [0, 'X']
  ],
  K: [
    [1,1],
    [1,0],
    [0,1],
    [-1,-1],
    [-1,0],
    [0,-1],
    [1,-1],
    [-1,1]
  ],
  B: [
    ['X','X']
  ],
  R: [
    ['X', 0],
    [0, 'X']
  ],
  N: [
    [-1,-2],
    [-2, -1],
    [-1,2],
    [-2,1],
    [1,-2],
    [2,-1],
    [1,2],
    [2,1]
  ]
};

const CalculatedAllTheMoves = {
  P: [],
  Q: [],
  K: [],
  B: [],
  R: [],
  N: [],
  '•' : [],
  'X' : []
};

function buildTheMoves() {
  for(let piece in OriginalAllTheMoves) {
    OriginalAllTheMoves[piece].forEach(move => {
      let mx = move[0];
      let my = move[1];

      if (mx === 'X' || my === 'X') {
        if (mx === 'X' && my === 'X') {
          for (let i = 1; i < 8 ; i++) {
            CalculatedAllTheMoves[piece].push([i,i]);
            CalculatedAllTheMoves[piece].push([i,-i]);
            CalculatedAllTheMoves[piece].push([-i,-i]);
            CalculatedAllTheMoves[piece].push([-i,i]);
          }
        } else if (mx === 'X') {
          for (let i = 1; i < 8 ; i++) {
            CalculatedAllTheMoves[piece].push([i,0]);
            CalculatedAllTheMoves[piece].push([-i,0]);
          }
        } else if (my === 'X') {
          for (let i = 1; i < 8 ; i++) {
            CalculatedAllTheMoves[piece].push([0,i]);
            CalculatedAllTheMoves[piece].push([0, -i]);
          }
        }
      } else {
        CalculatedAllTheMoves[piece].push(move)
      }
    });
  }
}

buildTheMoves();

function getMoves(from, showMe = false) {
  const whoami = board[from[0]][from[1]];
  const piece = pieces[whoami[1]];

  const dir = from[0] ? -1 : 1;
  let allMoves = [];
  let possibleMoves = [];


  CalculatedAllTheMoves[piece].forEach(move => {
      // console.log(from[0] + (move[0] * dir), from[0], (move[0] * dir))
      let x = from[0] + (move[0] * dir);
      let y = from[1] + move[1];
      let moveX = x >= 0 && x < 8;
      let moveY = y >= 0 && y < 8;
      allMoves.push([x, y])
      if (moveX && moveY) {
        possibleMoves.push([x, y]);
      }

  });

  if (showMe) {
    let oldBoard = JSON.parse(JSON.stringify(board));
    possibleMoves.forEach(m => {
      board[m[0]][m[1]] = [0, 7];
    });
    display(from);
    setTimeout(function () {
      board = oldBoard;
      display(from);
    }, 900);
  }

  return possibleMoves;
}

function displayMoves(moves) {
  let m = moves.map(m => {
    return rowN[m[0]] + (m[1]+1);
  });
  return m.join(', ');
}


function move(from, to) {
  const dir = from[0] ? 1 : -1;
  to[0] = to[0] * dir;
  const who = from[0] ? 'WHITE' : 'BLACK';
  console.log(who + ' is moving from ' + (rowN[from[0]] + '' + (from[1]+1)) + ' to ' + (rowN[to[0]] + '' + (to[1]+1)));
  console.error('can moves', displayMoves(getMoves(from)));
  swap(from, to);
}

function swap(from, to) {
  let orig = board[from[0]][from[1]];
  board[from[0]][from[1]] = zero();
  board[to[0]][to[1]] = orig;
}


function showTheBoard() {
  let current = [];
  board.forEach((row, ri) => {
    row.forEach((col, ci) => {
      if (col[1] > 0) {
        current.push([ri, ci]);
      }
    })
  });

  let weeee = setInterval(function () {
    if (current.length < 1) {
      clearInterval(weeee);
      return;
    }
    getMoves(current.pop(), true);
  }, 1000);
}


//
// getMoves([0,0], true);
// return;
//process.exit(0);


var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);
rl.setPrompt('guess> ');
rl.prompt();
rl.on('line', function(line) {

  if (line.substring(0,5) === 'moves') {
    if (line.substr(5,3)==='all') {
      showTheBoard();
    } else {
      let px = line.substr(5, 2).split('');
      let moves = getMoves([AlphaToNumber[px[0]], parseInt(px[1], 10) - 1], true);
      displayMoves(moves, true);
    }
  }

  if (line === "quit") rl.close();
  if (line.length === 4) {
    let px = line.split('');
    move([AlphaToNumber[px[0]],parseInt(px[1],10)-1], [AlphaToNumber[px[2]],parseInt(px[3],10)-1]);display();
  }
  rl.prompt();
}).on('close',function(){
  process.exit(0);
});

